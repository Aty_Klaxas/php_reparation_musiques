<?php declare(strict_types = 1);

const CHUNK = 100;

require_once __DIR__ . '/src/autoload.php';

use AtyKlaxas\AtyKlaxasCliMate;
use AtyKlaxas\Chrono;
use AtyKlaxas\CodecLongToExtensionHelper;
use AtyKlaxas\Ffmpeg;
use AtyKlaxas\Ffprobe;
use AtyKlaxas\Sanitize;
use AtyKlaxas\Screen;
use AtyKlaxas\Tree;

$help_description_array = [
    'Meeeeeeediiiiiiiiic !',
    'Meedic !',
    'Ah ! Medic !',
    'Docteur !',
    'Medic !',
    'Affiche le message d\'aide',
];

$climate = new AtyKlaxasCliMate();
$climate->description('CLI pour convertir des fichiers audio cassé dans leurs propre format');
$climate->arguments->add([
    'help' => [
        'prefix' => 'h',
        'longPrefix' => 'help',
        'description' => $help_description_array[array_rand($help_description_array)],
        'noValue' => true,
    ],
    'input' => [
        'prefix' => 'i',
        'longPrefix' => 'input',
        'description' => 'Dossier d\'entré',
        'required' => true,
    ],
    'output' => [
        'prefix' => 'o',
        'longPrefix' => 'output',
        'description' => 'Dossier de sortie',
        'required' => true,
    ],
    'no-screen' => [
        'longPrefix' => 'no-screen',
        'description' => 'Ne pas utiliser screen (si bugué)',
        'noValue' => true,
    ],
    'filter-ext' => [
        'longPrefix' => 'filter-ext',
        'description' => 'liste d\'extension a ignorer (jpg,png,bmp)',
    ],
]);
$climate->parse();

$input = Sanitize::path($climate->arguments->get('input'));
$output = Sanitize::path($climate->arguments->get('output'));

if (!file_exists($input)) {
    $climate->red('Invalid input file/dir');
    exit;
}

if (!is_dir($output)) {
    $climate->red('Invalid output directory');
    exit;
}

$climate->out('Making tree ...');

$line = null;
$chrono = null;
$inputs = Tree::tree($input, static function($i, $total, $dirs_remaining, $dir_count, $item) use (&$line, &$chrono) {
    // show line 1 line per second
    if (
        // chrono is here (first time not)
        $chrono instanceof Chrono &&
        // if its make less than 1 sec
        $chrono->timeFloat() < 1 &&
        // isn't the last line
        // NOT (is the last time)
        !($i === $total && $dirs_remaining === 0)
    ) {
        return;
    }

    if (!$chrono instanceof Chrono) {
        $chrono = new Chrono();
    } else {
        $chrono->start();
    }

    // clear last line
    if (!empty($line)) {
        echo "\r" . str_repeat(' ', strlen($line));
    }

    // create line
    $percent_file = safeDivide($i, $total) * 100;
    $percent_dir = safeDivide($dirs_remaining, $dir_count) * 100;

    if ($percent_file < 10) {
        $percent_file_offset = '00';
    } elseif ($percent_file < 100) {
        $percent_file_offset = '0';
    } else {
        $percent_file_offset = '';
    }

    if ($percent_dir < 10) {
        $percent_dir_offset = '00';
    } elseif ($percent_dir < 100) {
        $percent_dir_offset = '0';
    } else {
        $percent_dir_offset = '';
    }

    $percent_file = $percent_file_offset . number_format($percent_file, 2);
    $percent_dir = $percent_dir_offset . number_format($percent_dir, 2);

    // save and display line
    $line = $i . '/' . $total . ' ' . $percent_file . '% (' . $dirs_remaining . '/' . $dir_count . ' ' . $percent_dir . '%) ' . $item;
    echo "\r" . $line;
});
echo PHP_EOL;

$inputs = array_map(static function($input_item) use ($input) {
    return substr($input_item, strlen($input));
}, $inputs);

$climate->out('Making tree OK!');

if ($climate->arguments->defined('filter-ext')) {
    $climate->out('Filter extensions');
    $exts_to_filter = $climate->arguments->get('filter-ext');
    $exts_to_filter = explode(',', $exts_to_filter);

    $inputs = array_filter($inputs, static function($file) use ($exts_to_filter) {
        return !in_array(pathinfo($file, PATHINFO_EXTENSION), $exts_to_filter, true);
    });
}

if ($climate->arguments->defined('no-screen')) {
    $climate->out('Execute ffprobes');
} else {
    $climate->out('Prepare ffprobes');
}

/** @var Screen[] $screens_ffprobe */
$screens_ffprobe = [];
/** @var Ffprobe[] $ffprobes */
$ffprobes = [];
$number_of_fail_screen = 0;

foreach ($inputs as $relative_input) {
    $ffprobe = new Ffprobe();
    $ffprobe->setInput($input . $relative_input);

    if ($climate->arguments->defined('no-screen')) {
        exec($ffprobe->getCommand());
        $ffprobe->readOutput();
        $ffprobes[] = $ffprobe;
    } else {
        $screen = new Screen();
        $screen->setFfprobe($ffprobe);
        $screens_ffprobe[] = $screen;
    }
}

if (!$climate->arguments->defined('no-screen')) {
    $climate->out('Send chunks of screen (' . CHUNK . ')');
    $chunk_screens = array_chunk($screens_ffprobe, CHUNK);
    $chunk_screens_count = count($chunk_screens);

    foreach ($chunk_screens as $i => $chunk) {
        echo 'Running screens ' . $i . '/' . $chunk_screens_count;
        array_map(static function($screen) {
            /** @var Screen $screen */
            $screen->run();
        }, $chunk);

        echo ' Waiting for screens';
        Screen::WaitFor($chunk);

        foreach ($chunk as $screen) {
            /** @var Screen $screen */
            $ffprobe = $screen->getFfprobe();

            try {
                $ffprobe->readOutput();
            } catch (Throwable $t) {
                $number_of_fail_screen++;
                $climate->yellow('Number of screen fail: ' . $number_of_fail_screen);

                exec($ffprobe->getCommand());
                $ffprobe->readOutput();
            }

            $ffprobes[$ffprobe->getInput()] = $ffprobe;
        }

        $number_offset = 0;

        if ($i === 0) {
            $number_offset += 1;
        } else {
            $number_offset += floor(log10($i));
        }

        if ($chunk_screens_count === 0) {
            $number_offset += 1;
        } else {
            $number_offset += floor(log10($chunk_screens_count));
        }

        echo "\r" . str_repeat(' ', (int) (39 + $number_offset)) . "\r";;
    }

    echo PHP_EOL;
}

$array_codec_long_name = [];

foreach ($ffprobes as $ffprobe) {
    $data = $ffprobe->getOutput();
    $stream_selected = select_audio_stream($data['streams'] ?? []);

    if (empty($stream_selected)) {
        continue;
    }

    foreach ($stream_selected as $stream) {
        $array_codec_long_name[$stream['codec_long_name']] = $stream['codec_long_name'];
    }
}

$have_unsupported_codec = false;
$table_codec_transformation = [];

foreach ($array_codec_long_name as $codec_long_name) {
    $table_codec_transformation[$codec_long_name]['codec'] = $codec_long_name;
    $ext = CodecLongToExtensionHelper::codecLongNameToExt($codec_long_name);

    if ($ext === null) {
        $ext = "\e[41m" . 'null' . "\e[0m";
        $have_unsupported_codec = true;
    }

    $table_codec_transformation[$codec_long_name]['ext'] = $ext;
}

show_table($table_codec_transformation);

if ($have_unsupported_codec) {
    $climate->backgroundRed('ERREUR');
    $climate->out('la pré-analyse indique qu\'un element dans le lot n\'est pas supporté');
    $climate->out('Imposible d\'avancer plus');
    exit;
}

$climate->backgroundGreen('OK !');
$climate->green('la pré-analyse indique que tout les elements dans le lot sont supportés');
$climate->out('en avant pour la conversion');

$inputs = array_values($inputs);
$p = $climate->progress(count($inputs));
$climate->out('Create ffmpegs');
$screens = [];

foreach ($inputs as $i => $relative_input) {
    $ffprobe = $ffprobes[$input . $relative_input] ?? null;

    if (empty($ffprobe)) {
        $climate->red('ERROR: no ffprobe data');
        continue;
    }

    $data = $ffprobe->getOutput();
    $audios_stream = select_audio_stream($data['streams'] ?? []);

    if (empty($audios_stream) || !is_array($audios_stream)) {
        $climate->red('ERROR: no audio data');
        continue;
    }

    $audios_stream_count = count($audios_stream);

    if ($audios_stream_count > 1) {
        $climate->red('ERROR: too mush audio data (' . $audios_stream_count . ')');
        continue;
    }

    $audio_stream = array_shift($audios_stream);
    $ext = CodecLongToExtensionHelper::codecLongNameToExt($audio_stream['codec_long_name'] ?? '');

    $ffmpeg = new Ffmpeg();
    $ffmpeg->setInput($input . $relative_input);
    $ffmpeg->setOutput($output . $relative_input . '.' . $ext);

    $screen = new Screen();
    $screen->setFfmpeg($ffmpeg);

    $screens[] = $screen;
}

$climate->out('Conversion...');
$core_number = get_processor_cores_number();
// $core_number--;
$chunk_screens = array_chunk($screens, $core_number * 3);
$chunk_screens_count = count($chunk_screens);

foreach ($chunk_screens as $i => $chunk) {

    $number_offset = 0;

    if ($i === 0) {
        $number_offset += 1;
    } else {
        $number_offset += floor(log10($i));
    }

    if ($chunk_screens_count === 0) {
        $number_offset += 1;
    } else {
        $number_offset += floor(log10($chunk_screens_count));
    }

    echo str_repeat(' ', (int) (37 + $number_offset)) . "\r";
    echo 'Converting... ' . $i . '/' . $chunk_screens_count;
    array_map(static function($screen) use ($climate) {
        /** @var Screen $screen */
        $output = $screen->getFfmpeg()->getOutput();
        $output_dir = pathinfo($output, PATHINFO_DIRNAME);

        if (!file_exists($output_dir)) {
            mkdir($output_dir, 0777, true);
        }

        if (!is_dir($output_dir)) {
            $climate->red('ERROR: missing dir for ' . $output);
        } else {
            $screen->run();
        }
    }, $chunk);

    echo ' Waiting for screens';
    Screen::WaitFor($chunk);

    echo "\r";
}

echo PHP_EOL;
echo 'Done !' . PHP_EOL;
