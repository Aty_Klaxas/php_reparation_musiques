## Etape 0 - Préambule

Ce projet est un projet que j'ai fait sur un coup de tête  
Pas de stress, pas de contrainte, juste un truc qui fait ce que je veut comme je veut  
Un jour peut etre il deviendra pro, bien habillé et facile a utiliser, mais ce jour n'est pas encore arrivé

Donc si ça bug plante ou tue votre chien, je ne suis pas résponsable  
D'ailleurs si vous voulez jeter un coup d'oeuil dans le code pour comprendre ce que vous faites en fesant `php analyse.php`, je ne vous en empèche pas  


## Etape 1 - Installer les dépendances

- screen
- ffmpeg
- ffprobe (avec ffmpeg)
- php (>= 7.4 plz)
- composer

En général avec apt ça marche bien  
Pas besoin d'aller chercher compliqué just do:

```shell
sudo apt install screen php composer ffmpeg
```

Je ne vous install pas git puisque vous avez cloné ce repo  
Et il n'est pas utile dans la suite

## Etape 2 - Installer les dépendances composer

composer c'est le apt de php *en très vulgarisé*  
donc il faut instaler les truc qui sont decrit dans le composer.json dans la version du composer.lock

```shell
composer install # faire ça
#composer update # ne pas faire ça
```

## Etape 3 - Analyser
### Cas général

1. Savoir le dossier exact de l'analyse (`/somewhere/Music`)
2. Faire analyse.php avec --only-ext (voir --help) pour ne montrer que les extensions (`php analyse.php --only-ext -p /somewhere/Music`)
    1. Cela permet de savoir ce que l'on veut exclure rien qu'avec l'extention (exemple les .jpg/.png)
3. Faire une analyse avec --hide-table et --filter-ext puis une liste de type que l'on veut exclure (`php analyse.php --hide-table --filter-ext png,jpg -p /somewhere/Music`)
4. A la fin de l'analyse il y a une liste de tout les codec de stream, s'il en manque, il faut remonter ça au dévelopeurs

### Un peu plus de details

Pour voir ce qui en est  
indiquez obligatoirement ou analyser avec -p  
vous pouvez retirer les .jpg et ou .png et ou autre avec --filter-ext
vous pouvez ne pas utiliser `screen` avec --no-screen (*screen est un paquet pour linux* [*Voir Plus*](https://doc.ubuntu-fr.org/screen))

```shell
php analyse.php (--filter-ext jpg) (--no-screen) -p /Somewhere/Musique
```

Screen est un programme qui me permet ici de paralléliser les appels  
Je conseil de faire un fichier de log pour pouvoir voir les resultats, le output peut etre gros  
Comme ceci:

```shell
php analyse.php -p /Somewhere/Musique > /Somewhere/Musique.log
```

## Etape 4 - Faire griller des tartines sur le PC bien chaud

ça converts  
*TODO*

## TODOS:

- Dans l'analyse inclure les informations nesessaires seulement  
  Il y a bien trop d'information, des description, des lien, des PAVÉ de text  
  cela rend imbuvable le CSV
- Faire la partie conversion, le projet est la pour ça
- Faire la fonction de deduction de type