<?php declare(strict_types = 1);

const CHUNK = 100;

require_once __DIR__ . '/src/autoload.php';

use AtyKlaxas\AtyKlaxasCliMate;
use AtyKlaxas\Chrono;
use AtyKlaxas\CodecLongToExtensionHelper;
use AtyKlaxas\Ffprobe;
use AtyKlaxas\Screen;
use AtyKlaxas\Tree;

$help_description_array = [
    'Meeeeeeediiiiiiiiic !',
    'Meedic !',
    'Ah ! Medic !',
    'Docteur !',
    'Medic !',
    'Affiche le message d\'aide',
];

$climate = new AtyKlaxasCliMate();
$climate->description('CLI pour analyser récursivement les fichier');
$climate->arguments->add([
    'help' => [
        'prefix' => 'h',
        'longPrefix' => 'help',
        'description' => $help_description_array[array_rand($help_description_array)],
        'noValue' => true,
    ],
    'path' => [
        'prefix' => 'p',
        'longPrefix' => 'path',
        'description' => 'Ou analyser ?',
        'required' => true,
    ],
    'no-screen' => [
        'longPrefix' => 'no-screen',
        'description' => 'Paramètre pour ne pas utiliser screen (si bugué), tout vas etre fait a la suite, ce qui rend le script long',
        'noValue' => true,
    ],
    'filter-ext' => [
        'longPrefix' => 'filter-ext',
        'description' => 'Liste d\'extension a ignorer (jpg,png,bmp)',
    ],
    'only-ext' => [
        'longPrefix' => 'only-ext',
        'description' => 'Ne montrer que l\'analyse des extension',
        'noValue' => true,
    ],
    'extract-csv' => [
        'longPrefix' => 'extract-csv',
        'description' => 'Fichier to export',
        'castTo' => 'string',
    ],
    'hide-table' => [
        'longPrefix' => 'hide-table',
        'description' => 'Cacher la grosse table dans les logs',
        'defaultValue' => false,
        'castTo' => 'bool',
    ],
]);
$climate->parse();

if ($climate->arguments->defined('extract-csv')) {
    $csv_path = $climate->arguments->get('extract-csv');

    if (empty($csv_path)) {
        $climate->out('extract-csv take a parameter');
        exit;
    }

    $dir = pathinfo($csv_path, PATHINFO_DIRNAME);

    if (!file_exists($dir)) {
        $climate->out('extract-csv parameter directory don\'t exist');
        exit;
    }

    if (file_exists($csv_path)) {
        $climate->red('extract-csv file exist');
    }
}

$path = $climate->arguments->get('path');
$path = pathinfo($path);
$path = $path['dirname'] . '/' . $path['basename'];

if (empty($path)) {
    $climate->out('Empty parameter path');
    $climate->usage();
    exit;
}

$climate->out('Making tree ...');

$line = null;
$chrono = null;
$tree = Tree::tree($path, static function($i, $total, $dirs_remaining, $dir_count, $item) use (&$line, &$chrono) {
    // show line 1 line per second
    if (
        // chrono is here (first time not)
        $chrono instanceof Chrono &&
        // if its make less than 1 sec
        $chrono->timeFloat() < 1 &&
        // isn't the last line
        // NOT (is the last time)
        !($i === $total && $dirs_remaining === 0)
    ) {
        return;
    }

    if (!$chrono instanceof Chrono) {
        $chrono = new Chrono();
    } else {
        $chrono->start();
    }

    // clear last line
    if (!empty($line)) {
        echo "\r" . str_repeat(' ', strlen($line));
    }

    // create line
    $percent_file = safeDivide($i, $total) * 100;
    $percent_dir = safeDivide($dirs_remaining, $dir_count) * 100;

    if ($percent_file < 10) {
        $percent_file_offset = '00';
    } elseif ($percent_file < 100) {
        $percent_file_offset = '0';
    } else {
        $percent_file_offset = '';
    }

    if ($percent_dir < 10) {
        $percent_dir_offset = '00';
    } elseif ($percent_dir < 100) {
        $percent_dir_offset = '0';
    } else {
        $percent_dir_offset = '';
    }

    $percent_file = $percent_file_offset . number_format($percent_file, 2);
    $percent_dir = $percent_dir_offset . number_format($percent_dir, 2);

    // save and display line
    $line = $i . '/' . $total . ' ' . $percent_file . '% (' . $dirs_remaining . '/' . $dir_count . ' ' . $percent_dir . '%) ' . $item;
    echo "\r" . $line;
});
echo PHP_EOL;

$climate->out('Making tree OK!');

if ($climate->arguments->defined('filter-ext')) {
    $climate->out('Filter extensions');
    $exts_to_filter = $climate->arguments->get('filter-ext');
    $exts_to_filter = explode(',', $exts_to_filter);

    $tree = array_filter($tree, static function($file) use ($exts_to_filter) {
        return !in_array(pathinfo($file, PATHINFO_EXTENSION), $exts_to_filter, true);
    });
}

$extensions = [];
$screens = [];
$ffprobes = [];

$climate->out('Create screens, analyse 1/2');
foreach ($tree as $item) {
    $pinfo = pathinfo($item);
    $ext = $pinfo['extension'] ?? null;

    if (empty($extensions[$ext])) {
        $extensions[$ext] = 0;
    }

    $extensions[$ext]++;

    if ($climate->arguments->defined('only-ext')) {
        continue;
    }

    $ffprobe = new Ffprobe();
    $ffprobe->setInput($item);

    $screen = new Screen();
    $screen->setFfprobe($ffprobe);

    $ffprobes[] = $ffprobe;
    $screens[$screen->getScreenName()] = $screen;
}

if (!$climate->arguments->defined('only-ext')) {
    if ($climate->arguments->defined('no-screen')) {
        $climate->out('Manualy execute every ffprobe (slow)');
        $ffprobes_count = count($ffprobes);
        foreach ($ffprobes as $i => $ffprobe) {
            echo ($i + 1) . '/' . $ffprobes_count . "\r";
            exec($ffprobe->getCommand());
        }
        echo PHP_EOL;

        $screens = array_map(static function($ffprobe) {
            /** @var Ffprobe $ffprobe */

            return (new Screen())->setFfprobe($ffprobe);
        }, $ffprobes);
    } else {
        $climate->out('Send chunks of screen (' . CHUNK . ')');
        $chunk_screens = array_chunk($screens, CHUNK);
        $chunk_screens_count = count($chunk_screens);
        $screens = [];

        foreach ($chunk_screens as $i => $chunk) {
            echo 'Running screens ' . $i . '/' . $chunk_screens_count;
            array_map(static function($screen) {
                /** @var Screen $screen */
                $screen->run();
            }, $chunk);

            echo ' Waiting for screens';
            Screen::WaitFor($chunk);

            foreach ($chunk as $screen) {
                $screens[] = $screen;
            }

            $number_offset = 0;

            if ($i !== 0) {
                $number_offset += floor(log10($i));
            }

            if ($chunk_screens_count !== 0) {
                $number_offset += floor(log10($chunk_screens_count));
            }

            echo "\r" . str_repeat(' ', (int) (39 + $number_offset)) . "\r";
        }

        echo PHP_EOL;
    }
}

$climate->out('Manage screens to get displayable data (display if needed)');
// manage screens to get displayable data
$distincs = [];
$ffprobes_data = [];
$ffprobes_data_raw = [];
$screens_count = count($screens);

if (!$climate->arguments->defined('only-ext')) {
    foreach ($screens as $screen_key => $screen) {
        /** @var Screen $screen */
        $ffprobe = $screen->getFfprobe();

        try {
            $ffprobe->readOutput();
        } catch (Throwable $t) {
            echo ($screen_key + 1) . '/' . $screens_count . "\r";
            exec($ffprobe->getCommand());
            $ffprobe->readOutput();
        }

        $data = $ffprobe->getOutput();
        $ffprobes_data_raw[] = array_flatten_concat($data);

        $streams = $data['streams'] ?? [];
        $format = $data['format'] ?? [];
        unset(
            $data['streams'],
            $data['format'],
            $format['filename'],
        );

        $distincs['format_name'][$format['format_name'] ?? null] = $format['format_name'] ?? null;
        $distincs['format_long_name'][$format['format_long_name'] ?? null] = $format['format_long_name'] ?? null;

        foreach ($streams as $stream) {
            $distincs['streams_codec_name'][$stream['codec_name'] ?? null] = $stream['codec_name'] ?? null;
            $distincs['streams_codec_long_name'][$stream['codec_long_name'] ?? null] = $stream['codec_long_name'] ?? null;
        }

        $selected_stream = select_audio_stream($streams);

        if (is_array($selected_stream)) {
            foreach ($selected_stream as $stream) {
                $distincs['stream_selected_codec_name'][$stream['codec_name'] ?? null] = $stream['codec_name'] ?? null;
                $distincs['stream_selected_codec_long_name'][$stream['codec_long_name'] ?? null] = $stream['codec_long_name'] ?? null;
            }
        }

        $data['File'] = $ffprobe->getInput();
        $data['format'] = $format;

        foreach ($streams as $stream) {
            $fields = [
                'index',
                'codec_type',
                'codec_name',
                'codec_long_name',
                'bit_rate',
                'sample_rate',
                'channel_layout',
                'channels',
                'duration',
                'duration_ts',
            ];

            $stream_bis = $stream;
            $stream = [];

            foreach ($fields as $field) {
                $stream[$field] = $stream_bis[$field] ?? null;
            }

            unset($stream_bis);
            $data['stream'] = $stream;
            $ffprobes_data[] = array_flatten_concat($data);
        }
    }

    echo PHP_EOL;
}

$climate->out('OK!');

echo PHP_EOL;
echo 'Extensions by count: ' . PHP_EOL;

asort($extensions);
$extensions = array_reverse($extensions);
foreach ($extensions as $extension => $count) {
    echo $extension . ': ' . $count . PHP_EOL;
}

echo PHP_EOL;
echo 'Extensions by name: ' . PHP_EOL;

ksort($extensions);
foreach ($extensions as $extension => $count) {
    echo $extension . ': ' . $count . PHP_EOL;
}

if (!$climate->arguments->defined('only-ext')) {
    echo PHP_EOL . 'Distincs: ' . PHP_EOL . PHP_EOL;
    foreach ($distincs as $key => $array) {
        $lines = [];

        foreach ($array as $item) {
            $lines[] = [$key => $item];
        }

        show_table($lines);
        echo PHP_EOL;
    }

    $table_conversion = [];
    $impossible_to_convert = false;

    foreach ($distincs['stream_selected_codec_long_name'] as $codec_long_name) {
        $ext = CodecLongToExtensionHelper::codecLongNameToExt($codec_long_name);

        if ($ext === null) {
            $ext = "\e[41m" . 'null' . "\e[0m";
            $impossible_to_convert = true;
        }

        $table_conversion[] = [
            'stream_codec_long_name' => $codec_long_name,
            'extension_deducted' => $ext,
        ];
    }

    show_table($table_conversion);

    if ($impossible_to_convert) {
        $climate->backgroundRed('ERREUR');
        $climate->out('au moment ou j\'ecrit cette erreur, il n\'y a qu\'un cas possible');
        $climate->out('la fonction qui permet de dire codec => ext ne gère pas un codec');
    } else {
        $climate->backgroundGreen('OK !');
        $climate->out('pas d\'erreur detecté pour ce scan, tout vas bien =)');
    }
}

if ($climate->arguments->defined('extract-csv') && !empty($ffprobes_data) && !empty($ffprobes_data_raw)) {
    $path = $climate->arguments->get('extract-csv');
    $pinfo = pathinfo($path);
    $path_raw = $pinfo['dirname'] . '/' . $pinfo['filename'] . '.raw.' . $pinfo['extension'];

    // =====================================================

    $csv_data = [];
    $keys = [];

    foreach ($ffprobes_data as $ffprobes_datum) {
        $keys_loop = array_keys($ffprobes_datum);

        foreach ($keys_loop as $key) {
            $keys[$key] = $key;
        }
    }

    sort($keys);

    $csv_data[] = array_combine($keys, $keys);

    foreach ($ffprobes_data as $ffprobes_datum) {
        $line = [];

        foreach ($keys as $key) {
            $line[$key] = $ffprobes_datum[$key] ?? null;
        }

        $csv_data[] = $line;
    }

    arrayToCsv($csv_data, $path);

    // =====================================================

    $csv_data = [];
    $keys = [];

    foreach ($ffprobes_data_raw as $ffprobes_datum) {
        $keys_loop = array_keys($ffprobes_datum);

        foreach ($keys_loop as $key) {
            $keys[$key] = $key;
        }
    }

    sort($keys);

    $csv_data[] = array_combine($keys, $keys);

    foreach ($ffprobes_data_raw as $ffprobes_datum) {
        $line = [];

        foreach ($keys as $key) {
            $line[$key] = $ffprobes_datum[$key] ?? null;
        }

        $csv_data[] = $line;
    }

    arrayToCsv($csv_data, $path_raw);
}
