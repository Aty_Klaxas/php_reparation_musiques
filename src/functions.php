<?php declare(strict_types = 1);

function scandir_fullpath(string $path): array
{
    $filter = array_filter(scandir($path), static function($item) {
        return $item !== '.' && $item !== '..';
    });

    return array_map(static function($item) use ($path) {
        return $path . '/' . $item;
    }, $filter);
}

function tree(string $path): array
{
    if (!is_dir($path)) {
        return [$path];
    }

    $scan = scandir_fullpath($path);
    $dirs = [];
    $files = [];

    foreach ($scan as $full_path) {
        if (is_dir($full_path)) {
            $dirs[] = $full_path;
        }

        if (is_file($full_path)) {
            $files[] = $full_path;
        }
    }

    foreach ($dirs as $dir) {
        $sub_tree = tree($dir);

        foreach ($sub_tree as $file) {
            $files[] = $file;
        }
    }

    return $files;
}

function mb_str_pad($input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT, $encoding = null)
{
    $mb_len = !empty($encoding) ? mb_strlen($input, $encoding) : mb_strlen($input);
    $diff = strlen($input) - $mb_len;

    return str_pad($input, $pad_length + $diff, $pad_string, $pad_type);
}

function show_table(array $documents)
{
    $encoding = 'utf8';
    $documents = array_values($documents);
    $col_width = [];
    $keys = [];

    foreach ($documents as $document) {
        foreach ($document as $key => $item) {
            $keys[$key] = $key;

            if (is_string($item)) {
                $item = str_replace(PHP_EOL, '\\n', $item);
            }

            foreach ([$key, $item] as $to_len) {
                if (is_bool($to_len)) {
                    $to_len = $to_len ? 'True' : 'False';
                } else {
                    $to_len = (string) $to_len;
                }

                $len = mb_strlen($to_len, $encoding);

                if (!empty($col_width[$key]) && $col_width[$key] > $len) {
                    continue;
                }

                $col_width[$key] = $len;
            }
        }
    }

    array_unshift($documents, $keys);

    foreach ($documents as $i => $document) {
        $line = [];

        foreach ($keys as $key) {
            $col_len = $col_width[$key];

            if (is_string($document[$key] ?? null)) {
                $document[$key] = str_replace(PHP_EOL, '\\n', $document[$key]);
            }

            $line[] = mb_str_pad((string) ($document[$key] ?? ''), $col_len, ' ', STR_PAD_RIGHT, $encoding);
        }

        echo implode(' | ', $line) . PHP_EOL;

        if ($i !== 0) {
            continue;
        }

        $len = ((count($line) - 1) * 3) + array_sum(array_map('strlen', $line));

        if ($len > 0) {
            echo str_repeat('=', $len);
        }

        echo PHP_EOL;
    }
}

function array_flatten_concat($array): array
{
    if (!is_array($array)) {
        return $array;
    }

    $return = [];

    foreach ($array as $key => $item) {
        if (is_array($item)) {
            $sub_flat = array_flatten_concat($item);

            foreach ($sub_flat as $key2 => $item2) {
                $return[$key . '_' . $key2] = $item2;
            }
        } else {
            $return[$key] = $item;
        }
    }

    return $return;
}

function arrayToCsv(array $data, string $path)
{
    $fp = fopen($path, 'w');

    foreach ($data as $line) {
        fputcsv($fp, $line);
    }

    fclose($fp);
}

function see($data, $text = null): void
{
    echo "############ $text ############" . PHP_EOL;
    var_dump($data);
}

function select_audio_stream(array $streams): ?array
{
    $filter = array_filter($streams, static function($stream) {
        return ($stream['codec_type'] ?? null) === 'audio';
    });

    if (empty($filter)) {
        // well... no audio stream
        return null;
    }

    return $filter;
    // well... multiple audio stream, witch one use ?
    // $filter
}

/**
 * If denominator is 0 return 0 and avoid divide by 0 error
 *
 * @return void
 */
function safeDivide($nominator, $denominator): float
{
    if ($denominator == 0) {
        return 0;
    }

    return $nominator / $denominator;
}

function readline_confirm(): bool
{
    $read = readline('Continue ? [Yes/Oui]');
    $read = strtolower($read);

    return in_array($read, ['yes', 'oui', 'y', 'o'], true);
}

function get_processor_cores_number(): int
{
    $command = "cat /proc/cpuinfo | grep processor | wc -l";

    return  (int) shell_exec($command);
}
