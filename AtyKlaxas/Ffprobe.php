<?php declare(strict_types = 1);

namespace AtyKlaxas;

use RuntimeException;

class Ffprobe
{

    public static $BINARY = 'ffprobe';

    protected string $input = '';
    protected array $output;

    public function getCommand(): string
    {
        if (
            empty($this->input) ||
            empty(self::$BINARY)
        ) {
            return '';
        }

        $pos_quote = strpos($this->input, "'");
        $pos_dquote = strpos($this->input, '"');
        $quote = "'";

        if ($pos_quote !== false && $pos_dquote !== false) {
            $quote = '"';
            $this->input = str_replace('"', '\\"', $this->input);
        } elseif ($pos_quote !== false) {
            $quote = '"';
        }

        return self::$BINARY . ' -v quiet -print_format json -show_format -show_streams ' . $quote . $this->input . $quote . ' > ' . $this->getOutputPath();
    }

    public function getOutputPath(): string
    {
        $dir = __DIR__ . '/../Tmp';

        if (!file_exists($dir)) {
            if (!mkdir($dir, 0770, true) && !is_dir($dir)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
        }

        return $dir . '/' . preg_replace('/[^a-zA-Z0-9_]+/m', '_', $this->input) . '.json';
    }

    public function readOutput()
    {
        if (!file_exists($this->getOutputPath())) {
            if (empty($this->output)) {
                throw new RuntimeException('Error then getting output, command has been executed ?');
            }

            return;
        }

        $content = file_get_contents($this->getOutputPath());

        if (!is_string($content)) {
            $content = '';
        }

        $this->output = json_decode($content, true);
        unlink($this->getOutputPath());
    }

    /**
     * @return string
     */
    public function getInput(): string
    {
        return $this->input;
    }

    /**
     * @param string $input
     */
    public function setInput(string $input): void
    {
        $this->input = $input;
    }

    /**
     * @return array
     */
    public function getOutput(): array
    {
        return empty($this->output) ? [] : $this->output;
    }

    /**
     * @param array $output
     */
    public function setOutput(array $output): void
    {
        $this->output = $output;
    }

}
