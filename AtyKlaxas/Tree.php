<?php declare(strict_types = 1);

namespace AtyKlaxas;

/**
 * This is a sort of monster making everything it doesn't made for
 * I will make better objects for that
 */
class Tree
{

    private const CACHE = true;
    private const CACHE_PATH = __DIR__ . '/../cache';
    private const IS_FTCS = ['is_file', 'is_dir', 'is_readable'];

    /** @var string[][] */
    private static array $CACHED_SCAN = [];
    /** @var bool[][] */
    private static array $CACHED_IS_SOMETHING = [];
    private static bool $inited = false;

    public static function resolvePath(string $path): string
    {
        if ($path[0] !== '/') {
            $path = getcwd() . '/' . $path;
        }

        $expl = explode('/', $path);

        foreach ($expl as $i => $item) {
            if (substr($item, -1, 1) === '\\') {
                $expl[$i] .= '/' . $expl[$i + 1] ?? '';
                unset($expl[$i + 1]);
            }
        }

        foreach ($expl as $i => $item) {
            if ($item === '.') {
                unset($expl[$i]);
            }

            if ($item === '..') {
                unset($expl[$i], $expl[$i - 1]);
            }
        }

        return implode('/', $expl);
    }

    public static function init()
    {
        if (self::$inited === false) {
            self::$inited = true;

            if (self::CACHE === true) {
                self::$CACHED_SCAN = self::loadCacheFile(self::CACHE_PATH . '/scan.cache');

                foreach (self::IS_FTCS as $is_ftc) {
                    self::$CACHED_IS_SOMETHING[$is_ftc] = self::loadCacheFile(self::CACHE_PATH . '/' . $is_ftc . '.cache');
                }
            } else {
                self::$CACHED_SCAN = [];

                foreach (self::IS_FTCS as $is_ftc) {
                    self::$CACHED_IS_SOMETHING[$is_ftc] = [];
                }
            }
        }
    }

    public static function saveCacheFile(string $file, $content)
    {
        $dir = pathinfo($file, PATHINFO_DIRNAME);

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents($file, '');

        foreach ($content as $key => $value) {
            self::addCacheFile($file, $key, $value);
        }
    }

    public static function addCacheFile(string $file, string $path, $value)
    {
        $dir = pathinfo($file, PATHINFO_DIRNAME);

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents($file, $path . ':' . json_encode($value) . PHP_EOL, FILE_APPEND);
    }

    public static function loadCacheFile(string $file): array
    {
        if (!file_exists($file) || !is_readable($file)) {
            return [];
        }

        $content = file_get_contents($file);
        $expl1 = explode(PHP_EOL, $content);
        $return = [];

        foreach ($expl1 as $item) {
            if (empty($item)) {
                continue;
            }

            $expl2 = explode(':', $item, 2);
            $return[$expl2[0]] = json_decode($expl2[1] ?? '', true);
        }

        return $return;
    }

    public static function is_something(string $ftc, string $path): ?bool
    {
        if (!in_array($ftc, self::IS_FTCS, true)) {
            return null;
        }

        self::init();
        $resolved = self::resolvePath($path);
        $ptr = &self::$CACHED_IS_SOMETHING[$ftc][$resolved];
        $isset = isset($ptr);

        if (!$isset || self::CACHE === false) {
            $ptr = $ftc($resolved);

            if (!$isset && self::CACHE === true) {
                self::addCacheFile(self::CACHE_PATH . '/' . $ftc . '.cache', $resolved, $ptr);
            }
        }

        return $ptr;
    }

    public static function is_file(string $path): bool
    {
        return self::is_something('is_file', $path);
    }

    public static function is_dir(string $path): bool
    {
        return self::is_something('is_dir', $path);
    }

    public static function is_readable(string $path): bool
    {
        return self::is_something('is_readable', $path);
    }

    public static function scandir(string $path): array
    {
        self::init();
        $resolved = self::resolvePath($path);

        if (!self::is_dir($resolved)) {
            return [];
        }

        $isset = isset(self::$CACHED_SCAN[$resolved]);

        if (!$isset || self::CACHE === false) {
            self::$CACHED_SCAN[$resolved] = [];

            if (self::is_readable($resolved)) {
                foreach (scandir($resolved) as $item) {
                    if ($item === '.' || $item === '..') {
                        continue;
                    }

                    self::$CACHED_SCAN[$resolved][] = $resolved . '/' . $item;
                }
            }

            if (!$isset && self::CACHE === true) {
                self::addCacheFile(self::CACHE_PATH . '/scan.cache', $resolved, self::$CACHED_SCAN[$resolved]);
            }
        }

        return self::$CACHED_SCAN[$resolved];
    }

    /**
     * Make a tree with full path
     *
     * @param string $path Where to make tree
     * @param callable|null $callback Function with 3 parameter, the current item, the current total, the remaining actual directory, the item full path
     * @return array List of full path tree
     */
    public static function tree(string $path, ?callable $callback = null): array
    {
        if (self::is_file($path)) {
            return [$path];
        }

        $files = [];
        $dirs = [$path];
        $i = 0;
        $dir_count = 0;
        $total = 0;

        do {
            $scan = self::scandir(array_shift($dirs));
            $total += count($scan);

            foreach ($scan as $item) {
                if (self::is_file($item)) {
                    $files[] = $item;
                }

                if (self::is_dir($item)) {
                    $dirs[] = $item;
                    $dir_count++;
                }

                $i++;
                if (is_callable($callback)) {
                    $callback($i, $total, count($dirs), $dir_count, $item);
                }
            }
        } while(!empty($dirs));

        return $files;
    }

}
