<?php declare(strict_types = 1);

namespace AtyKlaxas;

class Ffmpeg
{

    public static $BINARY = 'ffmpeg';

    protected string $input = '';
    protected string $output = '';

    public function getCommand(): string
    {
        if (
            empty($this->input) ||
            empty($this->output) ||
            empty(self::$BINARY)
        ) {
            return '';
        }

        $replace = [
            ' ' => '\\ ',
        ];

        foreach ($replace as $replace_this => $by_this) {
            $this->input = str_replace($replace_this, $by_this, $this->input);
            $this->output = str_replace($replace_this, $by_this, $this->output);
        }

        $cmd = self::$BINARY . ' -y -i ' . $this->input . ' ' . $this->output;

        return $cmd;
    }

    public function getInput(): string
    {
        return $this->input;
    }

    public function setInput(string $input): Ffmpeg
    {
        $this->input = $input;
        return $this;
    }

    public function getOutput(): string
    {
        return $this->output;
    }

    public function setOutput(string $output): Ffmpeg
    {
        $this->output = $output;
        return $this;
    }

}
