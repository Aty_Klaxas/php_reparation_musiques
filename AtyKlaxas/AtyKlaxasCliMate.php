<?php declare(strict_types = 1);

namespace AtyKlaxas;

use League\CLImate\CLImate;
use League\CLImate\Exceptions\InvalidArgumentException;
use League\CLImate\TerminalObject\Dynamic\Progress;
use Throwable;

class AtyKlaxasCliMate extends CLImate
{

    public function parse(): void
    {
        if (!$this->arguments->exists('help')) {
            $this->arguments->add([
                'help' => [
                    'prefix' => 'h',
                    'longPrefix' => 'help',
                    'description' => 'Show the help message',
                    'noValue' => true,
                ],
            ]);
        }

        try {
            $this->arguments->parse();
        } catch (Throwable $t) {}

        if ($this->arguments->defined('help')) {
            $this->usage();
            exit;
        }

        if (!empty($t)) {
            if ($t instanceof InvalidArgumentException) {
                echo $t->getMessage() . PHP_EOL;
                exit;
            }

            throw $t;
        }
    }

    public function progress(int $total = null): Progress
    {
        return parent::progress($total);
    }

}
