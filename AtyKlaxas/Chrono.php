<?php declare(strict_types = 1);

namespace AtyKlaxas;

class Chrono
{

    protected float $microtime_start;
    protected ?float $microtime_stop = null;

    public function __construct() {
        $this->start();
    }

    public function start() {
        $this->microtime_start = microtime(true);
    }

    public function stop() {
        $this->microtime_stop = microtime(true);
    }

    public function isStopped(): bool {
        return $this->microtime_stop !== null;
    }

    public function timeDateInterval(): ?\DateInterval {
        $start_int = (int) $this->microtime_start;
        $stop_int = (int) $this->microtime_stop;

        $start_reste_float_micro_sec = (int) (($this->microtime_start - $start_int) * 1000000);
        $stop_reste_float_micro_sec = (int) (($this->microtime_stop - $stop_int) * 1000000);

        $start = new \DateTime();
        $start->setTimestamp($start_int);
        $start->modify("+$start_reste_float_micro_sec microseconds");

        $stop = new \DateTime();
        $stop->setTimestamp($stop_int);
        $stop->modify("+$stop_reste_float_micro_sec microseconds");

        $return = $start->diff($stop);

        return empty($return) ? null : $return;
    }

    public function timeFloat(): float {
        $stop = $this->microtime_stop ?? microtime(true);

        return $stop - $this->microtime_start;
    }

}
