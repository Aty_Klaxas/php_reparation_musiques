<?php declare(strict_types = 1);

namespace AtyKlaxas;

/**
 * Sanitise things with
 */
class Sanitize
{

    /**
     * Simply transform a path into
     * @return void
     */
    public static function path($path): string
    {
        if (!is_string($path)) {
            return '';
        }

        $slash_explode = explode('/', $path);

        if (count($slash_explode) <= 1) {
            return '';
        }

        $filter = array_filter($slash_explode);
        $implode = implode('/', $filter);

        if ($slash_explode[0] === '') {
            return '/' . $implode;
        }

        return $implode;
    }

}