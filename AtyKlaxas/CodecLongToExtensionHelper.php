<?php declare(strict_types = 1);

namespace AtyKlaxas;

class CodecLongToExtensionHelper
{

    public const SUPPORTED_CODEC_LONG_NAME = [
        'AAC (Advanced Audio Coding)' => 'aac',
        'ALAC (Apple Lossless Audio Codec)' => 'm4a',
        'MP3 (MPEG audio layer 3)' => 'mp3',
        'Opus (Opus Interactive Audio Codec)' => 'opus',
        'FLAC (Free Lossless Audio Codec)' => 'flac',
    ];

    public static function codecLongNameToExt(string $codec_long_name): ?string
    {
        return self::SUPPORTED_CODEC_LONG_NAME[$codec_long_name] ?? null;
    }

}
