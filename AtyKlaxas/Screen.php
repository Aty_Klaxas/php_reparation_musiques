<?php declare(strict_types = 1);

namespace AtyKlaxas;

class Screen
{

    protected $screen_name = '';
    protected $command = '';
    protected Ffprobe $ffprobe;
    protected Ffmpeg $ffmpeg;

    public function getFfprobe(): Ffprobe
    {
        return $this->ffprobe;
    }

    public function setFfprobe(Ffprobe $ffprobe): self
    {
        $this->ffprobe = $ffprobe;
        $this->setCommand($ffprobe->getCommand());
        $this->setScreenName($ffprobe->getInput() . '_' . microtime(true));

        return $this;
    }

    public function getFfmpeg(): Ffmpeg
    {
        return $this->ffmpeg;
    }

    public function setFfmpeg(Ffmpeg $ffmpeg): Screen
    {
        $this->ffmpeg = $ffmpeg;
        $this->setCommand($ffmpeg->getCommand());
        $this->setScreenName($ffmpeg->getInput() . '_' . $ffmpeg->getOutput() . '_' . microtime(true));

        return $this;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function setCommand(string $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getScreenName(): string
    {
        return $this->screen_name;
    }

    public function setScreenName(string $screen_name): self
    {
        $this->screen_name = preg_replace('/[^a-zA-Z0-9_]+/m', '_', $screen_name);

        if (strlen($this->screen_name) > 80) {
            $this->screen_name = hash('sha1', substr($this->screen_name, 0, 80)) . hash('sha1', substr($this->screen_name, 80));
        }

        return $this;
    }

    public function list(): array
    {
        exec('screen -ls', $out);

        array_shift($out);
        array_pop($out);
        array_pop($out);

        if (empty($out)) {
            return [];
        }

        return array_map(static function($item) {
            preg_match_all('/(?<screen_name>[0-9]+\.[a-zA-Z_0-9]+)/m', $item, $matches, PREG_SET_ORDER, 0);

            $matche = array_shift($matches);
            return $matche['screen_name'];
        }, $out);
    }

    public function exist(): bool
    {
        foreach ($this->list() as $item) {
            $exp = explode('.', $item);

            if ($exp[1] === $this->screen_name) {
                return true;
            }
        }

        return false;
    }

    public function run(): void
    {
        if (empty($this->screen_name) || empty($this->command)) {
            return;
        }

        $dquote = strpos($this->command, '"');

        if ($dquote !== false) {
            $this->command = str_replace('"', '\\"', $this->command);
        }

        $command = 'screen -dmS ' . $this->screen_name . ' sh -c "' . $this->command . '"';

        exec($command, $out);
    }

    /**
     * @param self[] $screens
     */
    public static function WaitFor(array $screens): void
    {
        do {
            foreach ($screens as $line => $screen) {
                if (
                    !$screen instanceof self ||
                    !$screen->exist()
                ) {
                    unset($screens[$line]);
                }
            }

            if (!empty($screens)) {
                sleep(1);
            }
        } while (!empty($screens));
    }

}
